import { call, put } from 'redux-saga/effects'
import CliforTypes from '../Redux/CliforRedux'

/**
 * Persiste um novo Clifor.
 * 
 * @param {*} api api injetada via /Sagas/index.j
 * @param {*} action action recebendo os dados do Clifor
 */
export function * createClifor (api, action) {
    const { clifor } = action
    const response = yield call(api.postClifor, clifor)
    if (response.ok) {
        const savedClifor = response.data
        yield put(CliforTypes.createCliforSuccess(savedClifor))
    } else {
        yield put(CliforTypes.createCliforFailure('deu ruim!'))
    }
}

/**
 * Atualiza um Clifor.
 * 
 * @param {*} api api injetada via /Sagas/index.j
 * @param {*} action action recebendo os dados do Clifor
 */
export function * updateClifor (api, action) {
    const { clifor, id } = action
    const response = yield call(api.putClifor, clifor, id)
    if (response.ok) {
        const savedClifor = response.data
        yield put(CliforTypes.updateCliforSuccess(savedClifor))
    } else {
        yield put(CliforTypes.updateCliforFailure(`Não foi possível atualizar o clifor ${id}`))
    }
}

/**
 * Consulta uma lista de Clifor.
 * 
 * @param {*} api api injetada via /Sagas/index.j
 * @param {*} action action recebendo parâmetros de consulta
 */
export function * getClifors (api) {
    const response = yield call(api.getClifors)
    if (response.ok) {
        const clifors = response.data
        yield put(CliforTypes.getCliforsSuccess(clifors))
    } else {
        yield put(CliforTypes.getCliforsFailure('Deu ruim!'))
    }
}

/**
 * Consulta um Clifor.
 * 
 * @param {*} api api injetada via /Sagas/index.j
 * @param {*} action action recebendo o id do Clifor
 */
export function * getClifor (api, { id }) {
    const response = yield call(api.getClifor, id)
    if (response.ok) {
        const clifor = response.data
        yield put(CliforTypes.getCliforSuccess(clifor))
    } else {
        yield put(CliforTypes.getCliforFailure(`Não foi possível retornar o clifor ${id}`))
    }
}

/**
 * Remove um Clifor,.
 * 
 * @param {*} api api injetada via /Sagas/index.j
 * @param {*} action action recebendo o id do Clifor
 */
export function * deleteClifor (api, action) {
    const { id } = action
    const response = yield call(api.deleteClifor, id)
    if (response.ok) {
        yield put(CliforTypes.deleteCliforSuccess('Clifor removido com sucesso'))
    } else {
        yield put(CliforTypes.deleteCliforFailure('Não foi possível remover o clifor!'))
    }
}
