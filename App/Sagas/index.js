import { takeLatest, all } from 'redux-saga/effects'
import API from '../Services/Api'
import FixtureAPI from '../Services/FixtureApi'
import DebugConfig from '../Config/DebugConfig'

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux'
import { CliforTypes } from '../Redux/CliforRedux'

/* ------------- Sagas ------------- */

import { startup } from './StartupSagas'
import {
  getClifor,
  getClifors,
  createClifor,
  updateClifor,
  deleteClifor
} from './CliforSagas'

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = DebugConfig.useFixtures ? FixtureAPI : API.create()

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield all([
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup),

    takeLatest(CliforTypes.GET_CLIFOR_REQUEST, getClifor, api),
    takeLatest(CliforTypes.GET_CLIFORS_REQUEST, getClifors, api),
    takeLatest(CliforTypes.CREATE_CLIFOR_REQUEST, createClifor, api),
    takeLatest(CliforTypes.UPDATE_CLIFOR_REQUEST, updateClifor, api),
    takeLatest(CliforTypes.DELETE_CLIFOR_REQUEST, deleteClifor, api)
  ])
}
