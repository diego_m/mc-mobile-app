import { StackNavigator } from 'react-navigation'
import CliforScreen from '../Containers/CliforScreen'
import CliforListScreen from '../Containers/CliforListScreen'
import LaunchScreen from '../Containers/LaunchScreen'

import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = StackNavigator({
  CliforScreen: { screen: CliforScreen },
  CliforListScreen: { screen: CliforListScreen },
  LaunchScreen: { screen: LaunchScreen }
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'LaunchScreen',
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default PrimaryNav
