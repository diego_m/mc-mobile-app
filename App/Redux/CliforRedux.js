import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  // get clifors
  getCliforsRequest: null,
  getCliforsSuccess: ['clifors'],
  getCliforsFailure: ['error'],
  // get clifor
  getCliforRequest: ['id'],
  getCliforSuccess: ['clifor'],
  getCliforFailure: ['error'],
  // create clifor
  createCliforRequest: ['clifor'],
  createCliforSuccess: ['clifor'],
  createCliforFailure: ['error'],
  // update clifor
  updateCliforRequest: ['clifor', 'id'],
  updateCliforSuccess: ['clifor'],
  updateCliforFailure: ['error'],
  // delete clifor
  deleteCliforRequest: ['id'],
  deleteCliforSuccess: null,
  deleteCliforFailure: ['error']
})

export const CliforTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: false,
  saving: false,
  error: null,
  clifors: [],
  clifor: null
})

/* ------------- Selectors ------------- */

/* ------------- Reducers ------------- */

// get clifors
export const getCliforsRequest = (state) =>
  state.merge({ fetching: true, clifors: [] })
export const getCliforsSuccess = (state, { clifors }) =>
  state.merge({ fetching: false, error: null, clifors })
export const getCliforsFailure = (state, { error }) =>
  state.merge({ fetching: false, error, clifors: [] })

// get clifor
export const getCliforRequest = (state) =>
  state.merge({ fetching: true, clifor: null })
export const getCliforSuccess = (state, { clifor }) =>
  state.merge({ fetching: false, error: null, clifor })
export const getCliforFailure = (state, { error }) =>
  state.merge({ fetching: false, error, clifor: null })

// create clifor
export const createCliforRequest = (state) =>
  state.merge({ saving: true, clifor: null })
export const createCliforSuccess = (state, { clifor }) =>
  state.merge({ saving: false, error: null, clifor })
export const createCliforFailure = (state, { error }) =>
  state.merge({ saving: false, error, clifor: null })

// update clifor
export const updateCliforRequest = (state) =>
  state.merge({ fetching: true, clifor: null })
export const updateCliforSuccess = (state, { clifor }) =>
  state.merge({ fetching: false, error: null, clifor })
export const updateCliforFailure = (state, { error }) =>
  state.merge({ fetching: false, error, clifor: null })

// delete clifor
export const deleteCliforRequest = (state) =>
  state.merge({ fetching: true })
export const deleteCliforSuccess = (state) =>
  state.merge({ fetching: false, error: null })
export const deleteCliforFailure = (state, { error }) =>
  state.merge({ fetching: false, error })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  // get clifors
  [Types.GET_CLIFORS_REQUEST]: getCliforsRequest,
  [Types.GET_CLIFORS_SUCCESS]: getCliforsSuccess,
  [Types.GET_CLIFORS_FAILURE]: getCliforsFailure,
  // get clifor
  [Types.GET_CLIFOR_REQUEST]: getCliforRequest,
  [Types.GET_CLIFOR_SUCCESS]: getCliforSuccess,
  [Types.GET_CLIFOR_FAILURE]: getCliforFailure,
  // create clifor
  [Types.CREATE_CLIFOR_REQUEST]: createCliforRequest,
  [Types.CREATE_CLIFOR_SUCCESS]: createCliforSuccess,
  [Types.CREATE_CLIFOR_FAILURE]: createCliforFailure,
  // update clifor
  [Types.UPDATE_CLIFOR_REQUEST]: updateCliforRequest,
  [Types.UPDATE_CLIFOR_SUCCESS]: updateCliforSuccess,
  [Types.UPDATE_CLIFOR_FAILURE]: updateCliforFailure,
  // delete clifor
  [Types.DELETE_CLIFOR_REQUEST]: deleteCliforRequest,
  [Types.DELETE_CLIFOR_SUCCESS]: deleteCliforSuccess,
  [Types.DELETE_CLIFOR_FAILURE]: deleteCliforFailure
})
