const moment = require('moment')

export const dateFormat = (date) => moment(date).format('DD/MM/Y [às] hh:mm[h]')

export const now = () => moment()
