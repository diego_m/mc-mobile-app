import React, { Component } from 'react'
import { ScrollView, Text, Image, View } from 'react-native'
import { Images } from '../Themes'

// Styles
import styles from './Styles/LaunchScreenStyles'

export default class LaunchScreen extends Component {

  componentDidMount () {
    setTimeout(() => this.props.navigation.navigate('CliforListScreen'), 2000)
  }

  render () {
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <ScrollView style={styles.container}>
          <View style={styles.centered}>
            <Image source={Images.launch} style={styles.logo} />
            <Text style={styles.sectionText}>
              MINICURSO MOBILE
            </Text>
          </View>

          <View style={styles.section} >
            <Text style={styles.sectionText}>
              Este app foi desenvolvido no minicurso de mobile, e está disponível para qualquer um baixá-lo e "brincar"
            </Text>
          </View>

        </ScrollView>
      </View>
    )
  }
}
