import React, { Component } from 'react'
import {
  ScrollView,
  Text,
  FlatList,
  KeyboardAvoidingView,
  View,
  Alert,
  ActivityIndicator,
  TouchableOpacity
} from 'react-native'
import { Header, Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import CliforActions from '../Redux/CliforRedux'

// Styles
import styles from './Styles/CliforListScreenStyle'
import Colors from '../Themes/Colors'

import { dateFormat } from '../Lib/Utils'

class CliforListScreen extends Component {

  static defaultProps = {
    clifors: [],
    title: 'Clifors'
  }
  static propTypes = {
    clifors: PropTypes.array,
    title: PropTypes.string
  }

  constructor (props) {
    super(props)
    this.state = {
      clifors: []
    }
  }

  componentDidMount () {
    this.props.getClifors()
  }

  componentWillReceiveProps (newProps) {
    if (newProps.clifors) {
      console.log(newProps.clifors)
    }
  }

  _onPressNewClifor () {
    this.props.navigation.navigate('CliforScreen')
  }

  _onPressRefresh () {
    this.props.getClifors()
  }

  _renderEmpty = () => <Text style={styles.label}>Nenhum clifor encontrado</Text>

  _renderSeparator = () => <View style={styles.separator}></View>

  _keyExtractor = (item, index) => index

  _renderRow (row) {
    const clifor = row.item
    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate('CliforScreen', { clifor })}>
        <View style={styles.rowClifor}>
          <View style={{ flex: 1 }}>
            <Text>{clifor.nmClifor}</Text>
            <Text>{clifor.cpfCnpj}</Text>
          </View>
          <Icon name='chevron-right' style={styles.rowCliforIcon} />
        </View>
      </TouchableOpacity>)
  }

  render () {
    if (this.props.fetching) {
      return (
        <View style={styles.spinnerContainer}>
          <ActivityIndicator size='large' />
        </View>)
    }

    const rightComponent = (
      <TouchableOpacity
        title='Atualizar'
        icon={{ name: 'refresh', type: 'font-awesome' }}
        onPress={this._onPressRefresh.bind(this)} />
    )

    return (
      <View>
        <Header
          centerComponent={{ text: this.props.title, style: { color: '#fff' } }}
          rightComponent={rightComponent} />
        <FlatList
          data={this.props.clifors}
          renderItem={this._renderRow.bind(this)}
          keyExtractor={this._keyExtractor}
          initialNumToRender={20}
          ListHeaderComponent={this._renderHeader}
          ListFooterComponent={this._renderFooter}
          ListEmptyComponent={this._renderEmpty}
          ItemSeparatorComponent={this._renderSeparator} />
        <View>
          <Button
            title='Novo Clifor'
            icon={{ name: 'plus', type: 'font-awesome' }}
            onPress={this._onPressNewClifor.bind(this)} />
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    fetching: state.clifor.fetching,
    error: state.clifor.error,
    clifors: state.clifor.clifors
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getClifors: () => dispatch(CliforActions.getCliforsRequest()),
    removeClifor: (id) => dispatch(CliforActions.deleteCliforRequest(id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CliforListScreen)
