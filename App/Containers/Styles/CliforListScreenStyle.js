import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics } from '../../Themes/'
import Colors from '../../Themes/Colors';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  title: {
    textAlign: 'center',
    fontSize: 20
  },
  titleIcon: {
    marginHorizontal: Metrics.baseMargin
  },
  titleContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: Metrics.baseMargin
  },
  rowClifor: {
    justifyContent: 'center',
    flexDirection: 'row',
    paddingVertical: Metrics.smallMargin,
    paddingHorizontal: Metrics.baseMargin
  },
  rowCliforIcon: {

  },
  spinnerContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  separator: {
    height: 1,
    backgroundColor: Colors.coal
  }
})
