import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, TextInput, Button } from 'react-native'
import { connect } from 'react-redux'
import CliforActions from '../Redux/CliforRedux'

// Styles
import styles from './Styles/CliforScreenStyle'

class CliforScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {}
    const { clifor } = this.props.navigation.state.params
    if (clifor) {
      this.state = {
        ...this.state,
        id: clifor._id,
        nmClifor: clifor.nmClifor,
        cpfCnpj: clifor.cpfCnpj
      }
    }
  }

  componentWillReceiveProps (newProps) {
    // if (!newProps.saving) {
    // }
  }

  _onPressSave () {
    const { id, nmClifor, cpfCnpj } = this.state
    const clifor = {
      nmClifor,
      cpfCnpj
    }
    if (id) {
      this.props.updateClifor(clifor, id)
    } else {
      this.props.createClifor(clifor)
    }
  }
  
  render () {
    const { nmClifor, cpfCnpj } = this.state
    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior='position'>
          <TextInput
            placeholder='Nome do clifor'
            value={nmClifor}
            onChangeText={nmClifor => this.setState({ nmClifor })} />
          <TextInput
            placeholder='CPF/CNPJ'
            value={cpfCnpj}
            onChangeText={cpfCnpj => this.setState({ cpfCnpj })} />
          <Button
            title='Salvar'
            onPress={this._onPressSave.bind(this)} />
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }
}

const mapStateToProps = ({ clifor }) => {
  return {
    saving: clifor.saving,
    clifor: clifor.clifor
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    createClifor: (clifor) => dispatch(CliforActions.createCliforRequest(clifor)),
    updateClifor: (clifor, id) => dispatch(CliforActions.updateCliforRequest(clifor, id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CliforScreen)
